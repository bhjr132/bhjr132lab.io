---
title: Emergency Relief Spending Act 2018
introducer: UncookedMeatloaf
party: Greens
portfolio: Deputy Prime Minister
assent: 2018-02-07
urls:
- "https://reddit.com/r/ModelNZParliament/comments/7umlt4/b24_emergency_relief_spending_bill_2018_urgent/"
---

#Emergency Relief Spending Act 2018

**1. Purpose**

An act to rebuild areas devastated by Cyclone Ella.

**2. Title and Commencement**

1) This Act shall be referred to as the *Emergency Relief Spending Act* or the *ESRA* in short.

2) This Act shall take effect upon its passage into law.

3) This Act shall expire exactly 20 calendar months from its passage unless re-authorized by the Parliament.

## Part 1: Structure

**3. Appropriations**

1) $1,000,000,000 NZD is appropriated for cyclone relief and community rebuilding to be used as specified in later sections of this Act.

> a) $700,000,000 is appropriated for the purposes of individual relief as outlined in Section 5.
> 
> b) $300,000,000 is appropriated to local councils in Auckland, Northland, Bay of Plenty, Manukau, and Waikato-Matangi as outlined in Section 6.

2) $875,000 NZD is appropriated to operate the Cyclone Emergency Assessment Office as outlined in Section 4 for a period of 18 calendar months.

**4. Cyclone Emergency Assessment Office**

1) There is created an agency of the Ministry of the Interior responsible for cyclone damage assessments, known as the "Cyclone Emergency Assessment Office" and managed by a director appointed by the Minister for the Interior.

2) The Cyclone Emergency Assessment Office is compelled to employ individuals responsible for surveying and assessing cyclone damage for the purposes of determining relief funding allotment.

3) The Office is compelled to conduct a complete survey of all settlements and areas with a census-determined population of greater than 10 persons. The Office is also compelled to conduct a conduct a survey of all residential locations not surveyed where occupants have applied for aid.

## Part 2: Application

**5. Individual Relief**

1) All residents of Northland, Auckland, Bay of Plenty, Manukau, and Waikato-Matangi may apply for emergency relief, drawn from this fund.

2) Applicants must meet the following requirements:

> a) An official damage appraisal from the Cyclone Emergency Assessment Office:
> 
> > i. After insurance of greater than 7% the applicant's annual combined household income.

3) Aid recipients must use the money expressly for damages assessed by the survey; after a three-month period the Office shall conduct a re-audit of all aid recipients to ensure that no money has been misspent.

> a) Any individual found to be mishandling or misspending relief funds shall be fined $3,000 NZD or 50% the value of the relief grant, whichever is more.

**6. Relief to Local Councils**

1) Funds appropriated to local councils shall be used to rebuild local infrastructure.

2) Councils may apportion the funds at their discretion.

3) Funding shall be appropriated based on an appraisal by the Cyclone Emergency Assessment Office.

> a) Funding shall be prioritized to councils with the lowest local budget, highest damage appraisal, and lowest average household income.

4) Exactly three months after reciept of funding, the Office shall conduct an audit of all councils to ensure that relief grant money is being spent only on local infrastructure. 

> a) If a local council is found not in compliance, any individuals responsible shall be fined $3,000 or 5% the value of the relief grant, whichever is more.
