---
title: Parihaka Day Act 2018
introducer: imnofox
party: Greens
portfolio: Business
assent: 2018-03-10
urls:
- "https://reddit.com/r/ModelNZParliament/comments/82hl5s/b21_parihaka_day_bill_2018_third_reading/"
amends:
- Holidays Act 2003
---

#Parihaka Day Act 2018

**1. Purpose**

The purpose of this Act is to create a national holiday to recognise the invasion of the peaceful settlement of Parihaka on the 5th of November 1881 by the government. The village of Parihaka was founded as a place of refuge for Māori, as their lands were being confiscated by the crown. This day will contribute to the national recognition of the New Zealand wars.

**2. Parihaka Day to be a day of commemoration**

1) In commemoration of the invasion of Parihaka on 5 November 1881, 5 November in each year shall be known as Parihaka Day.

**3. Observance of Parihaka Day**

1) Parihaka Day shall be observed as a public holiday throughout New Zealand on the day of the week on which it falls.

**4. Amendments to Holidays Act 2003**

1) The Holidays Act 2003 is amended in the manner indicated in the Schedule.

**5. Relationship with Holidays Act 2003**

1) This Act is subject to section 45A of the Holidays Act 2003 (which concerns the transfer of the public holiday for the purposes of Part 2, subpart 3 of that Act when Parihaka Day falls on a Saturday or a Sunday).

## Schedule: Amendments to Holidays Act 2003

**Section 44(1)**  

Insert the following paragraph after paragraph (h):

***

> (ha) Parihaka Day:

***

**Section 45A**  

Replace the heading with “Transfer of Waitangi Day, ANZAC Day, and Parihaka Day public holidays”.

**Section 45A (1)**  

Replace “Waitangi Day or ANZAC Day” with “Waitangi Day, ANZAC Day, or Parihaka Day”.

**Section 45A (2)**  

Replace “Waitangi Day or more than 1 public holiday for ANZAC Day” with “each of Waitangi Day, ANZAC Day, and Parihaka Day”.
