---
title: Compulsory Te Reo Language Act 2018
bill: 23
introducer: AnswerMeNow1
party: Māori
portfolio: Member's Bills
assent: 2018-05-02
urls:
- "https://reddit.com/r/ModelNZParliament/comments/8ewk0z/b23_compulsory_te_reo_m%C4%81ori_bill_2018_third/"
---

#Compulsory Te Reo Language Act 2018

**1. Purpose**

The purpose of this Act is to institute compulsory education of the Te Reo Māori language as a second language across New Zealander government-run primary schools by the year 2023.

**2. Title and Commencement**

1)	This Act may be cited as the Compulsory Te Reo Language Act 2018, or shortened as the Te Reo Language Act 2018.

2)	Section 4 will go into effect the day after the date on which this Act receives Royal assent, and be considered void when there are enough Te Reo teachers to be 1 senior Te Reo teacher in every school.

3)	Sections 5 and 6 will go into effect on the day after the date on which this Act receives Royal assent.

**3. Interpretation**

In this Act, unless otherwise expressly stated—

> **schools** refer to government-run primary public schools in New Zealand.
>
> **Te Reo** and **Māori** both refer to the Māori language.

**4. Incentivising schools**

1)	$11,000,000 will be appropriated annually by Parliament to fund new scholarships for Trainee Te Reo teachers.

**5. Instituting compulsory Māori education**

1)	Compulsory Te Reo education shall be added to the school curriculum.

> a) The Te Reo curriculum must be developed in conjunction with Māori and should be based upon the existing Te Aho Arataki Marau mō te Ako i Te Reo Māori - Kura Auraki curriculum.

3) Every school must take reasonable steps to provide complusory Te Reo education.

4) Te Kotahitanga programme will be reinstated, for which $10,000 will be appropriated annually by Parliament to support change in schools to better support Te Reo education.

**6. Secondary Schools**

1)	Secondary schools shall not be required to teach Māori as a compulsory course for students sitting NCEA.

2)	The language must be offered to students as an option for a course by the year 2030.
