---
title: Appropriation (May - July 2018 Estimates) Act 2020
bill: 48
introducer: toastinrussian
party: National
portfolio: Finance
assent: 2018-05-06
commencement: 2018-05-07
urls:
- "https://reddit.com/r/ModelNZParliament/comments/8gt7ic/b48_appropriation_mayjuly_2018_estimates_bill/"
repeals:
- C0002A00004
---

#Appropriation (May - July 2018 Estimates) Act 2020

**1. Title**

This Act is the Appropriation (May - July 2018 Estimates) Act.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Application**

The Act applies to the May - July 2018 financial year, which will end on the 31st July 2018.

**[Schedule 1 - Appropriations for the May - July 2018 Financial Year](https://docs.google.com/document/d/e/2PACX-1vR2hLITRl5pSMQBn3A8y_NGzSuei7_D9NdNgtFB8aHgjGKpmDBjHCwQ2HKH6hnl5NDjohKH9kKnBlHV/pub)**
