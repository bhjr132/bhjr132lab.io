---
title: Crimes (Abortion Reform) Amendment Act 2018
bill: 33
introducer: UncookedMeatloaf
party: Greens
portfolio: Member's Bills
assent: 2018-05-26
commencement: 2018-05-27
urls:
- "https://reddit.com/r/ModelNZParliament/comments/8kuxff/b33_crimes_abortion_reform_amendment_bill_third/"
amends:
- Crimes Act 1961
---

#Crimes (Abortion Reform) Amendment Act 2018

**1. Purpose**

The purpose of this Act is to liberalise abortion laws by allowing it to be lawful for any reason given the woman or girl is consenting.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Crimes Act 1961 (the **principal Act**).

**4. Section 187A amended (Meaning of unlawfully)**

1) Replace Section 187A (1) with the following:

***

> (1) For the purposes of sections 183 and 186, any act specified in either of those sections is done unlawfully unless, in the case of a pregnancy of not more than 20 weeks’ gestation, the person doing the act has full informed and enthusiastic consent from the patient.

***
