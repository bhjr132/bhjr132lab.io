---
title: Crown Minerals (Protection of World Heritage Sites) Amendment Act 2018
bill: 40
introducer: imnofox
party: Greens
portfolio: Member's Bills
assent: 2018-09-01
commencement: 2018-09-02
urls:
- "https://reddit.com/r/ModelNZParliament/comments/987fqb/b40_crown_minerals_protection_of_world_heritage/"
amends:
- Crown Minerals Act 1991
---

#Crown Minerals (Protection of World Heritage Sites) Amendment Act 2018

**1. Title**

This Act is the Crown Minerals (Protection of World Heritage Sites) Amendment Act 2018

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Crown Minerals Act 1991 (the **principal Act**).

**4. Section 2 amended (Interpretation)**

In section 2(1), insert in its appropriate alphabetical order:

***

> **World Heritage Site** means a site included in the World Heritage List by the World Heritage Committee under paragraph 2 of Article 11 of the Convention Concerning the Protection of the World Cultural and Natural Heritage 1972.

***

**5. Schedule 4 amended (Land to which access restrictions apply)**

In Schedule 4, after clause 14, insert the clause set out in the Schedule.

## Schedule

**Schedule 4 amended**  

In Schedule 4, after clause 14, insert:

***

> *15* All areas that are World Heritage Sites.

***
