---
title: Safe Access to Reproductive Services Act 2019
bill: 96
introducer: AnswerMeNow1
party: Te Tawharau
portfolio: Member's Bills
assent: 2019-01-10
commencement: 2019-01-11
first_reading: reddit.com/r/ModelNZParliament/comments/a1vyf9/b96_safe_access_to_reproductive_services_bill/
committee: reddit.com/r/ModelNZParliament/comments/a5slhc/b96_safe_access_to_reproductive_services_bill/
final_reading: reddit.com/r/ModelNZParliament/comments/acctae/b96_safe_access_to_reproductive_services_bill/
---

# Safe Access to Reproductive Services Act 2019

## Explanatory Note

**General policy statement**

This bill endeavours to create 'no protest zones' around premises providing reproductive health services, similar to those enforced in some Australian states.

In Tasmania and Victoria, it is an offence to film, intimidate, and protest within 150m of clinics providing reproductive health services.

These kinds of protests can have significant impacts on patients, especially those undergoing terminations. It is inappropriate and harmful to allow protestors to intimidate and target those accessing reproductive health services, especially when it can be such a difficult and emotional experience already.

**Clause by clause analysis**

* *Clause 1* is the title clause.

* *Clause 2* is the commencement clause. It provides for the Bill to come into force on the day after the date on which it receives the Royal assent.

* *Clause 3* is the purpose clause.

* *Clause 4* provides that the Bill will bind the Crown.

* *Clause 5* is the interpretation clause.

* *Clause 6* makes it an offence for any person to engage in prohibited behaviour in a safe access zone.

* *Clause 7* makes it an offence to distribute a recording of a person accessing, or attempting to access, premises that provide reproductive health services, without that other person’s consent.

* *Clause 8* provides for police officers to seize materials used or about to be used to commit an offence unser section 6 or section 7.

**1. Title**

This Act is the Safe Access to Reproductive Services Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this Act is to provide for safe access zones around premises offering reproductive health services.

**4. Act binds the Crown**

This Act binds the Crown.

**5. Interpretation**

In this Act,-

**distribute** includes-

> a) communicate, display, send, supply or transmot, whether to a particular person or not; and
> 
> b) make available for access, whether by a particular person or not; and
> 
> c) enter into an agreement to do anything mentioned in the preceding paragraph (a) or (b); and
> 
> d) attempt to distribute

**prohibited behaviour** means-

> a) in relation to a person, harassing, intimidating, interfering with, hindering, threatening, obstructing or impeding that person; or
> 
> b) communicating in relation to reproductive health services in a manner that is able to be seen or heard by a person access, or attempting to access, RH premises; or
> 
> c) interfering with or impeding a footpath or entrance way; or
> 
> d) intentionally recording another person accessing or attempting to access RH premises at which reproductive health services are provides, without that person's consent; or
> 
> e) any other prescribed behaviour

**reproductive health services** means services relating to advice, medication, and treatment in respect of reproductive health, including the prevention and termination of pregnancy

**RH premises** means premises at which reproductive health services are provided

**safe access zone** means an areas within a radius of 175 metres from RH premises

**6. Prohibited behaviour**

1) Every person commits an offence who engages in prohibited behaviour within a safe access zone.

2) A person who commits an offence against subsection (1) is liable on conviction to a fine not exceeding $40,000, or a prison term not exceeding 6 months.

3) A person does not engage in prohibited behaviour within a safe access zone by intentionally recording another person accessing or attempting to access RH premises without that other person's consent if the person making the recording-

> a) is a police officer acting in the course of the officer's duties as a police officer, and the officer's conduct is reasonable for the performance of those duties; or
> 
> b) is employed or contracted to provide services at the RH premises.

**7. Offence to distribute recording**

1) Every person commits an offence who, without lawful excuse, publishes or distributes a recording of a person accessing, or attempting to access, RH premises, without that other person's consent.

2) A person who commits an offence against subsection (1) is liable on conviction to a fine not exceeding $40,000 or a prison term not exceeding 6 months.

**8. Seizure of material used in an offence**

1) If a police officer suspects on reasonable grounds that a person is committing, or is likely to commit, an offence against section 6 or section 7, the officer may seize and remove any object, material, document, information, poster, picture, or recording that the officer believes on reasonable grounds was used, or is about to be used, in relation to the offence or likely offence.
