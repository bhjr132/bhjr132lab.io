---
title: Shop Trading Hours (Restricted Trading Hours) Amendment Act 2019
bill: 97
introducer: WillShakespeare99
party: Labour
portfolio: Member's Bills
assent: 2019-01-17
commencement: 2019-01-18
first_reading: reddit.com/r/ModelNZParliament/comments/9w0zpb/b94_prohibition_of_conversion_therapy_bill_first/
committee: reddit.com/r/ModelNZParliament/comments/9yxmfa/b94_prohibition_of_conversion_therapy_bill/
final_reading: reddit.com/r/ModelNZParliament/comments/adqm52/b97_shop_trading_hours_restricted_trading_hours/
amends:
- Shop Trading Hours Act 1990
---

# Shop Trading Hours (Restricted Trading Hours) Amendment Act 2019

**1. Title**

This Act is the Shop Trading Hours (Restricted Trading Hours) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it received the Royal Assent.

**3. Purpose**

The purpose of this Act is to restore restricted trading days, to ensure that all workers are guaranteed holidays on days that are of national significant interest.

**4. Principal Act amended.**

This Act amends the Shop Trading Hours Act 1990 (the **principal Act**).

**5. New Part 1 inserted (Restricted trading days)**

After section 2A, insert the following new Part 1:

***

> ## Part 1: Restricted trading days
> 
> **3. Shops to be closed on Anzac Day morning, Good Friday, Easter Sunday,  Parihaka Day, and Christmas Day**
> 
> 1) Subject to sections 4, 4A, and 4B, every shop shall remain closed-
> 
> > a) before 1 pm on Anzac Day; and
> > 
> > b) all day on any day that is Good Friday, Easter Sunday, or Christmas Day.
> 
> 2) For the purposes of subsection (1)-
> 
> > a) a shop that has a common entrance with a factory or warehouse is not closed at any time unless-
> > 
> > > i) the shop’s entrance is then closed; and
> > > 
> > > ii) no person in the shop is then selling goods, canvassing for orders of goods, or delivering goods; and
> > 
> > b) subject to paragraph (a), a shop is not closed at any time unless-
> > 
> > > i) the shop is then locked, or otherwise secured against entry by members of the public; and
> > > 
> > > ii) no person in the shop is then selling goods, canvassing for orders of goods, or delivering goods.
>
> **4. Certain shops may remain open**
> 
> 1) Section 3(1) does not apply to a shop if it is-
> 
> > a) a shop where-
> > 
> > > i) the goods for sale include nothing that is not food, drink, a household item, a personal item, an automotive fuel, an automotive lubricant, an automotive part, or an automotive accessory, of a kind that people may reasonably need to be able to buy at any time; and
> > > 
> > > ii) the quantity of goods for sale is no greater than that sufficient to meet the demands of the people who live or are staying in the area where the shop is, and people (other than people travelling in order to buy goods at the shop) travelling through the area; or
> > 
> > b) a shop whose principal business is selling goods falling into one or other of the following categories:
> > 
> > > i) goods intended to be bought as souvenirs:
> > > 
> > > ii) duty free goods (that is to say goods sold from or through an export warehouse or a duty-free store (as those terms are defined in section 5(1) of the Customs and Excise Act 2018) to people intending to take the goods out of New Zealand):
> > > 
> > > iii) prepared or cooked food ready to be eaten immediately in the form in which it is sold; or
> > 
> > c) a shop at any public passenger transport terminal, or at any station where public passenger transport services stop, whose principal business is selling goods falling into one or other of the following categories:
> > 
> > > i) books, magazines, and newspapers:
> > > 
> > > ii) the categories specified in paragraph (b); or
> > 
> > d) a pharmacy; or
> > 
> > e) a shop in premises where a bona fide exhibition or show devoted (entirely or primarily) to agriculture, art, industry, and science, or any of those matters, is being held.
> 
> 2) Section 3(1) does not apply to a shop in respect of its remaining closed at any time on any day if-
> 
> > a) on 31 July 1990 there was in force in respect of the area in which the shop is situated an order under section 20 of the repealed Act (applied for under section 18(2) of that Act) authorising shops to be open at that time on that day; and
> > 
> > b) all conditions (if any) subject to which the order was made are being (or, as the case may be, have been) complied with.
>
> **5. Offence**
> 
> 1) The occupier of a shop that is not closed in accordance with section 3 commits an offence against this Act, and is liable, on conviction, to a fine not exceeding $1,000.
> 
> 2) For the purposes of subsection (1),-
> 
> > a) the occupier of a shop,-
> > 
> > > i) includes any agent, manager, supervisor, or other person acting or apparently acting in general management or control of the shop; and
> > > 
> > > ii) where the shop is occupied by a corporation or body of persons (whether incorporated or not), includes the shop’s working manager:
> > 
> > b) a hawker or other person carrying on business by selling goods, or offering goods for sale by retail, otherwise than in a shop who-
> > 
> > > i) sells goods otherwise than in a shop; or
> > > 
> > > ii) offers goods for sale otherwise than in a shop; or
> > > 
> > > iii) delivers goods to a customer otherwise than in a shop,-
> > 
> > is deemed to be the occupier of a shop that is not closed.
> 
> 3) In any proceedings for an offence against this Act,-
> 
> > a) it is sufficient to allege in the charging document that a place is a shop and to state in the charging document the name of its ostensible occupier, or a style or title under which its occupier is usually known or carries on business; and
> > 
> > b) the charge may from time to time be amended as to the actual name of the occupier of the place.

***

**6. Part 2 repealed (Trading on Easter Sunday)**

Part 2 is repealed.
