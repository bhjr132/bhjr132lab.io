---
title: Prostitution Reform (Immigration) Amendment Act 2019
bill: 99
introducer: eelsemaj99
party: Opportunities
portfolio: Internal Affairs
assent: 2019-01-06
commencement: 2019-01-07
first_reading: reddit.com/r/ModelNZParliament/comments/a1m72d/b98_prostitution_reform_immigration_amendment/
committee: reddit.com/r/ModelNZParliament/comments/a447h6/b99_prostitution_reform_immigration_amendment/
final_reading: reddit.com/r/ModelNZParliament/comments/a8dgnn/b98_prostitution_reform_immigration_amendment/
amends:
- Prostitution Reform Act 2003
---

# Prostitution Reform (Immigration) Amendment Act 2019

**1. Title**

This Act is the Prostitution Reform (Immigration) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it received the Royal Assent.

**3. Purpose**

The purpose of this Act is to remove the abolition of sex workers gaining visas under the Immigration Act 2009.

**4. Principal Act amended**

This Act amends the Prostitution Reform Act 2003 (the **principal Act**).

**5. Sections repealed**

Repeal section 19 of the principal Act.
