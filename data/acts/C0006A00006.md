---
title: KiwiSaver (Compassionate Consideration) Amendment Act 2019
bill: 119
introducer: Mattsthetic
author:
- Mattsthetic (National)
party: National
portfolio: Member's Bills
assent: 2019-02-28
commencement: 2019-03-01
first_reading: reddit.com/r/ModelNZParliament/comments/ao01cw/b119_kiwisaver_compassionate_consideration/
committee: reddit.com/r/ModelNZParliament/comments/aq29yf/b119_kiwisaver_compassionate_consideration/
final_reading: reddit.com/r/ModelNZParliament/comments/atcoz4/b119_kiwisaver_compassionate_consideration/
amends:
- KiwiSaver Act 2006
---

# KiwiSaver (Compassionate Consideration) Amendment Act 2019

**1. Title**

This Act is the KiwiSaver (Compassionate Consideration) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this Act is to amend the KiwiSaver Act 2006 so that an individual with
a life-shortening medically-certified illness or condition may apply for compassionate consideration, in order to make withdrawals from their KiwiSaver account prior to reaching age 65.

**4. Principal Act**

This Act amends the KiwiSaver Act 2006 (the **principal Act**).

**5. Schedule 1 amended**

1) In Schedule 1, after clause 12, insert:

***

> **12A. Withdrawal in cases of limited life expectancy**
> 
> 1) If the manager (in the case of a restricted KiwiSaver scheme) or the supervisor (in the case of any other KiwiSaver scheme) is reasonably satisfied that a member has a chronic illness or a health condition that causes them to have a limited life expectancy, the member may, on application to that manager or supervisor in accordance with clause 13, make a limited life expectancy withdrawal in accordance with this clause.
> 
> 2) The amount of that limited life expectancy withdrawal may be up to the value of the member’s accumulation.
> 
> 3) In this clause, limited life expectancy means that the person is highly likely to die before the age of 65 years despite available medical care and treatment.
> 
> 4) The manager (in the case of a restricted KiwiSaver scheme) or the supervisor (in the case of any other KiwiSaver scheme)—
> 
> > a) must be reasonably satisfied that the withdrawal reflects the following aims—
> > 
> > > i) encouraging a long-term saving habit and asset accumulation while the individual is working:
> > > 
> > > ii) providing for the individual’s well-being and financial independence:
> > 
> > b) may direct that the amount withdrawn be limited to a specified amount that, in the opinion of the manager (in the case of a restricted KiwiSaver scheme) or the supervisor (in the case of any other KiwiSaver scheme), is required to reflect the aims in subsection (a).

***

2) In Schedule 1, in the heading to clause 13, delete “for significant financial hardship or serious illness”.

3) In Schedule 1, in clause 13(1), insert “or 12A ”after “clause 10 or 12”.
