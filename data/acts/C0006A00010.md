---
title: Crown Minerals Amendment Act 2019
bill: 126
introducer: imnofox
author:
- imnofox (Greens)
party: Greens
portfolio: Environment
assent: 2019-03-15
commencement: 2019-03-16
first_reading: reddit.com/r/ModelNZParliament/comments/augff0/b126_crown_minerals_amendment_bill_first_reading/
committee: reddit.com/r/ModelNZParliament/comments/awqe0l/b126_crown_minerals_amendment_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/ayz4ya/b126_crown_minerals_amendment_bill_final_reading/
amends:
- Crown Minerals Act 1991
---

# Crown Minerals Amendment Act 2019

**1. Title**

This Act is the Crown Minerals Amendment Act 2019.

**2. Commencement**

This Act comes into force the day after the day it receives the Royal assent.

**3. Principal Act amended**

This Act amends the Crown Minerals Act 1991 (the **principal Act**).

**4. Section 2 amended (Interpretation)**

In section 2(1), insert the following definitions in the appropriate alphabetical order:

***

> **residential dwelling** means a building or group of buildings, or part of a building or group of buildings, that is-
> 
> > i) used, or intended to be used, only or mainly for residential purposes; and
> > 
> > ii) occupied, or intended to be occupied, exclusively as the home or residence of not more than 1 household
> 
> **fracking** means the injection of fluid into shale beds at high pressure in order to free up petroleum resources

***

**5. Section 25 amended (Grant of permit)**

After section 25(2), insert the following:

***

> 2A) The Minister must not grant a mining permit for minerals in respect of land that is within 300 metres of any residential dwelling.

***

**6. Section 23A amended (Application for permits)**

In section 23A, insert as subsection (2):

***

> 2) However, a person may not apply under this section for a mining permit for petroleum if intending to use the method of fracking.

***
