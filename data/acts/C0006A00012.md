---
title: Electricity (Renewables) Amendment Act 2019
bill: 124
introducer: imnofox
author:
- imnofox (Greens)
party: Greens
portfolio: Environment
assent: 2019-03-18
commencement: 2019-03-19
first_reading: reddit.com/r/ModelNZParliament/comments/atcruo/b124_electricity_renewables_amendment_bill_first/
committee: reddit.com/r/ModelNZParliament/comments/axuvps/b124_electricity_renewables_amendment_bill/
final_reading: reddit.com/r/ModelNZParliament/comments/b04fej/b124_electricity_renewables_amendment_bill_final/
amends:
- Electricity Act 1992
---

# Electricity (Renewables) Amendment Act 2019

**1. Title**

This Act is the Electricity (Renewables) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act amended**

This Act amends the Electricity Act 1992.

**4. New Part 6A inserted**

The following Part is inserted after Part 6:

***

> ## Part 6A - Restriction on new fossil-fuelled thermal electricity generating capacity
> 
> **62A. Purpose of this Part**
> 
> The purpose of this Part is to reduce the impact of fossil-fuelled thermal electricity generation on the climate crisis by placing a restriction on new baseload fossil-fuelled thermal electricity generation capacity until April 2030.
> 
> **62B. Expiry of this Part**
> 
> This Part expires on the close of 1 April 2030.
> 
> **62C. Interpretation**
> 
> In this Part, unless the context otherwise requires-
> 
> **Authority** means the Electricity Authority established by section 12 of the Electricity Industry Act 2010
> 
> **co-generation process** means a process for the combined production of electricity and useful thermal energy
> 
> **commencement capacity** means, in relation to an existing plant, the rated generating capacity of the plant immediately before the commencement of this Part (determined according to its nameplate or nameplates)
> 
> **distribution network** means the electricity lines, and associated equipment, owned or operated by an electricity distributor, but does not include-
> 
> > a) the national grid; or
> > 
> > b) an embedded network that is used to convey less than 2.5 GWh per annum
> 
> **exemption** means an exemption granted under this Part
> 
> **existing plant**-
> 
> > a) means an electricity generation plant that is in operation at the commencement of this Part; and
> > 
> > b)includes an electricity generation plant that was commissioned before the commencement of this Part that, at that commencement,-
> > 
> > > i) is temporarily not in operation (for example, as a result of maintenance being carried out on the plant); or
> > > 
> > > ii)is not in operation but is available to come back into operation at less than 1 week's notice
> 
> **fossil fuel** means any of the following:
> 
> > a) natural gas:
> > 
> > b) coal (as defined in section 2(1) of the Crown Minerals Act 1991):
> > 
> > c) petroleum:
> > 
> > d) any refined petroleum product:
> > 
> > e) any other obligation fuel (as defined in the Climate Change Response Act 2002)
> 
> **greenhouse gas** has the same meaning as in section 4 of the Climate Change Response Act 2002
> 
> **nameplate** has the same meaning as in section 3 of the Electricity Industry Reform Act 1998
> 
> **natural gas** means-
> 
> > a) all gaseous hydrocarbons produced from wells, including wet gas and residual gas remaining after the extraction of condensate from wet gas; and
> > 
> > b) liquid hydrocarbons, other than condensate, extracted from wet gas and sold as natural gas liquids, for example, liquid petroleum gas; and
> > 
> > c) coal seam gas
> 
> **operate** does not include the provision only of maintenance and related services
> 
> **petroleum** has the same meaning as in section 2(1) of the Crown Minerals Act 1991
> 
> **specified generation plant**-
> 
> > a) means an electricity generation plant that—
> > 
> > > i) uses, or will use, fossil fuels if fossil fuels provide, or will provide, more than 20% of the total fuel energy input for the generator or generators constituting the plant in any 12-month period of operation commencing on the first day of operation or an anniversary of that date; and
> > > 
> > > ii) has, or will have, a rated generating capacity (determined according to its nameplate or nameplates) of more than 10 MW; but
> > 
> > b) does not include-
> > 
> > > i) an existing plant; or
> > > 
> > > ii) an electricity generation plant that is operating in accordance with a contract for reserve energy entered into by, or on behalf of, the Commission; or
> > > 
> > > iii) an electricity generation plant that is declared by regulations not to be a specified generation plant
> 
> **start-up time** means, in relation to a plant, the time that is necessary for the plant to be generating electricity at its rated generating capacity (determined according to its nameplate or nameplates) from a cold start where the generator or generators constituting the plant start generating electricity after having previously been completely shut down and not generating.
> 
> **62D. Restriction on connection and operation of specifed generation plant**
> 
> 1) No preson may connect a specified generation plant to the national grid or a distribution network, or operate a specified generation plant, unless-
> 
> > a) the person has an exemption in relation to the plant; and 
> > 
> > b) the plant is connected and operated in accordance with the exemption.
> 
> 2) Every person who breaches subsection (1) commits an offence and is liable on summary conviction to a fine not exceeding $550,000.
> 
> 3) If a person is convicted of an offence under this section, the court may, in addition to any penalty that the court may impose under subsection (2), order that person to pay an amount not exceeding 2 times the value of anu commercial gain resulting from the breach of that section if the court is satisfied that the breach occurred in the course of producing a commercial gain.
> 
> **62E. Minister may grant exemption**
> 
> 1) The Minister may, by notice in the *Gazette*, exempt a person from the restriction in section 62D in respect of a specified generation plant.
> 
> 2) The Minister may only grant an exemption that-
> 
> > a) implements the effect of a recommendation of the Authority (including in relation to terms and conditions); and
> > 
> > b) does not differ from that recommendation in any material way.
> 
> 3) The Minister may only grant an exemption if they are satisfied that 1 or more of the following applies:
> 
> > a) the specified generation plant will be a non-baseload plant that will-
> > 
> > > i) have an average load factor less than a prescribed limit; and
> > > 
> > > ii) emit greenhouse gases less than a prescribes limit; and
> > > 
> > > iii) have a start-up time less than a prescribed limit; or
> > 
> > b) the specified generation plant will be necessary for the purpose of mitigating the effects of an emergency (whether present or future); or
> > 
> > c) the specified generation plant will be necessary for the purpose of forming part of a co-generation process that will operate above an energy efficiency level prescribed for the purposes of this paragraph; or
> > 
> > d) the specified generation plant will be connected and operated in circumstances where an existing thermal electricity generation plant will be retired in whole or in part and the specified generation plant together with any part of the existing thermal electricity generation plant that is not retired will be operated in a manner that-
> > 
> > > i) will reduce greenhouse gas emissions by at least 20% (based on emissions of the existing thermal electricity generation plant and projected emissions assessed in the prescribed manner); and
> > > 
> > > ii) will not reduce security of supply below prescribed margins (as assessed in the prescribed manner).
> 
> 4) The Minister must specify in the exemption the ground or purpose under subsection (3) under which the exemption is granted.
> 
> 5) The Minister of Energy may grant an exemption on any terms and conditions he or she reasonably considers are necessary to give effect to the purpose of this Part.
>
> **62F. Public consultation**
> 
> 1) Before making a recommendation to the Minister to grant an exemption, the Authority must-
> 
> > a) publish its intention to do so and invite members of the public and interested organisations to give their views on the matter; and
> > 
> > b) give a reasonable opportunity to those persons to give those views; and
> > 
> > c) have regard to those views
> 
> 2) Subsection (1) does not apply if the exemption relates to section 62E(3)(b) and the Authority reasonably considers that consultation is not desirable in the circumstances.
> 
> **62G. Recovation of exemption**
> 
> 1) The Miniser may, by notice in the *Gazette* on the recommendation of the Authority, revoke an exemption if they consider that-
> 
> > a) the specified generation plant is no longer required for the purpose for which the exemption was granted; or
> > 
> > b) the specified generation plant has not been, or is not being, operated in accordance with the terms and conditions of the exemption.
> 
> 2) The Authority may exempt a person from the restriction in section 62D in respect of a specified generation plant if the Authority is satisfied that the specified generation plant is necessary or desirbale for the purposes of mitigating the effects of a present emergency.
> 
> 3) The Authority may grant an exemption under subsection (12) on any terms and conditions it reasonable considers are necessary to give effect to the purpose of that subsection.
> 
> 4) An exemption under subsection (2), unless it is sooner revoked, remains in force for the period stated in it, not exceeding 3 months.
>   
> **62G. Regulations for purposes of this Part**
> 
> 1) The Governor-General may, by Order in Council made on the recommendation of the Minister, make regulations for all or any of the following purposes:
> 
> > a) declaring an electricity generation plant not to be a specified generation plant:
> > 
> > b) prescribing limits for average load factors, greenhouse gas emissions, and start-up times for the purposes of section 62G:
> > 
> > c) specifying how those average load factors, greenhouse gas emissions, and start-up times are to be calculated or determined:
> > 
> > d) prescribing matters for the purposes of sections 62C(3) and 62G(1)(c\) to (e), (2), and (3):
> > 
> > e) prescribing how applications for exemptions are to be made and dealt with:
> > 
> > f) prescribing matters that the Minister of Energy must have regard to when granting, varying, or revoking an exemption:
> > 
> > g) authorising the Commission to require payment by an applicant for an exemption of any costs incurred by the Commission in connection with the exercise or performance by the Commission of any power or function relating to the application:
> > 
> > h) authorising the Commission to modify the terms and conditions of exemptions (including prescribing when and how the Commission may act and the criteria that the Commission must comply with).

***
