---
title: Correctional Apprenticeships Act 2019
bill: 128
introducer: hk-laichar
author:
- hk-laichar (Labour)
party: Labour
portfolio: Justice
assent: 2019-03-21
commencement: 2019-03-21
first_reading: reddit.com/r/ModelNZParliament/comments/awqhmp/b128_correctional_apprenticeships_bill_first/
committee: reddit.com/r/ModelNZParliament/comments/ayz5a4/b128_correctional_apprenticeships_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/b1ar81/b128_correctional_apprenticeships_bill_final/
---
