---
title: Ivory Act 2019
bill: 209
introducer: LeChevailerMal-Fait
author: 
- LeChevailerMal-Fait (National)
party: National
portfolio: Member's Bills
assent: 2019-11-08
commencement: 2020-02-08
first_reading: reddit.com/r/ModelNZParliament/comments/dkxz3p/b209_ivory_bill_first_reading/
committee: reddit.com/r/ModelNZParliament/comments/dnpf6u/b209_ivory_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/dqg3va/b209_ivory_bill_final_reading/
---

# Ivory Act 2019

**1. Title**

This Act may be cited as the Ivory Act 2019.

**2. Commencement**

This Act comes into force 3 months after Royal Assent..

**3. Purpose**

The purpose of this Act is to combat the illegal ivory market by creating a system of licenses for the importation or exportation of ivory and other transactions of raw or worked ivory.

**4. Application**

This Act applies only to New Zealand.

**5. Interpretation**

In this Act—

* **ivory item** means an item which is made of ivory or that has ivory in it;

* **raw ivory** means ivory that is unaltered from its original form before it was reshaped for artistic, fashion, decorative or other purposes;

* **worked ivory** means ivory which has been redrafted or crafted into something other than raw ivory;

* the **Minister** means the Minister for the environment.

**6. Act to bind the Crown**

This Act binds the Crown.

**7. Prohibition and Offence**

1) A person commits an offence if they—

> a) buy,
> 
> b) sell,
> 
> c) hire,
> 
> d) export, or
> 
> e) import

an uncertified ivory item, or otherwise keep or offer (including through advertisement) any such item for the above.

2) A person commits an offence if they cause or  facilitate an offence under subsection  (1).

3) In this section, “uncertified ivory” means ivory which has no certificate or where no certificate is being produced as set out in section 8 or 9.

4) An ivory item is exempt from this section if the item was crafted from ivory obtained before 1989. 

5) A person commits an offence under this section in relation to an item only if they know or suspect or ought to know or suspect that the item contains ivory.

6) It is a defence for a person charged under this section to prove that the person took all reasonable precautions and exercised due diligence to avoid committing an offence .

7) A person guilty of an offence under this section is liable— 

> a) on summary conviction, to —
> > 
> > i) imprisonment for a term not exceeding 6 months; or
> > 
> > ii) a fine not exceeding 5 times the value of the goods to which the offence relates; or
> > 
> > iii) both
> 
> b) on conviction on indictment, to 
> 
> > i) imprisonment for a term not exceeding 6 months; or
> > 
> > ii) a fine not exceeding 5 times the value of the goods to which the offence relates; or
> > 
> > iii) both

8) A body corporate guilty of an offence under this section is liable to a fine not exceeding 5 times the value of the goods to which the offence relates.

9) For a body corporate to be guilty of an offence under this section it is sufficient that an offence under subsection (1) or (2)  have been committed by an individual in the course of their duties for the body corporate.

**8. Certificates from prescribed organisations**

1) The Minister may add organisations to the list of prescribed organisations who may grant certificates required in section 7.

2) Such an organisation may issue a certificate for a lawful acquisition of—

> a) raw ivory;
> 
> b) worked ivory,

3) When adding organisations to the list of prescribed organisations the Minister must consider whether—

> a) the organisation has lawful jurisdiction over ivory within their territory,
> 
> b) the organisation is transparent,
> 
> c) the organisation meets anti-corruption standards,
> 
> d) the organisation uses sufficiently rigorous measures to prevent counterfeiting and forgery,
> 
> e) the ivory that the organisation certifies only humanely procured ivory,
> 
> f) if the ivory was sourced post 1989 that it must is sourced from only populations of that are not endangered
> 
> g) the organisation meets any additional requirements set out by the Minister.

4) The Minister may from time to time publish a list of non-endangered populations for the purposes of (3)(f).

**9. Domestic Certificates**

1) The Minister may issue certificates for a lawful acquisition of worked ivory required in section 7 for ivory items redrafted or crafted in the Aotearoa New Zealand.

2) Any person applying for a certificate from the Minister must provide—

> a) the name and address of the owner of the item,
> 
> b) a description of the item,
> 
> c) a description of any distinguishing features of the item,
> 
> d) the weight of ivory contained in the finished item,
> 
> e) the weight of ivory lost in manufacturing the item,
> 
> f) a photograph or photographs of the item showing all distinguishing features,
> 
> g) the certificate for a lawful acquisition of worked ivory or the certificate for a lawful acquisition of raw ivory for the ivory from which the item has been redrafted or crafted,
> 
> h) information about any trade with the item that is expected to take place,
> 
> i) other information specified in regulation made by the Minister, and
> 
> j) payment of any fee required by regulation made by the Minster.

3) The Minister may reject an application if it—

> a) does not meet the requirements under subsection (2), 
> 
> b) provides false information, or
> 
> c) does not meet requirements in regulation made by the Minister.

**10. The certificate**

A certificate for a lawful requisition of worked ivory in section 8 or 9 must include—

> a) a unique combination of letters and figures, 
> 
> b) enough information necessary to identify the item, and
> 
> c) the unique combination of letters and figures of all previous certificates from parent sources. 

**11. Duty to report**

1) If the owner of an ivory item becomes aware that information on its certificate has or will become inaccurate or incomplete or the certificate is inaccurate or incomplete the owner must notify the Minister.

2) The owner must provide the missing or accurate information.

3) If the information is unobtainable it must be noted on the certificate. 

4) The Minister may revoke the certificate if the owner has failed to comply with this section.

5) A ivory item that is—

> a) bought,
> 
> b) sold,
> 
> c) hired,
> 
> d) exported, or
> 
> e) imported

in a condition that is inaccurate with respect to its certificate is to be treated as a “uncertified” ivory item.
