---
title: Smoke-free Environments (Illicit Trade) Amendment Act 2019
bill: 212
introducer: CrusherCollins2020
author: 
- LeChevalierMal-Fait (National)
party: ACT
portfolio: Health
assent: 2019-11-17
commencement: 2019-11-17 (Part 6, 2024-11-17)
first_reading: reddit.com/r/ModelNZParliament/comments/dp3umj/b212_smokefree_environments_illicit_trade/
committee: reddit.com/r/ModelNZParliament/comments/drut7p/b212_smokefree_environments_illicit_trade/
final_reading: reddit.com/r/ModelNZParliament/comments/duoymj/b212_smokefree_environments_illicit_trade/
amends:
- Smoke-free Environments Act 1990
---

# Smoke-free Environments (Illicit Trade) Amendment Act 2019

**1. Title**

This Act may be cited as the Smoke-free Environments (Illicit Trade) Amendment Act 2019.

**2. Commencement**

1) This Act apart from Part 6 comes into force on Royal Assent.

2) Part 6 comes into force 5 years after Royal Assent.

3) The Minister may by regulations amend the coming into force date to a date less than 5 years after Royal Assent.

**3. Purpose**

The purpose of this Act is to bring New Zealand into compliance with the Protocol to Eliminate Illicit Trade in Tobacco Products and for connected purposes.

**4. Interpretation**

* The **principal Act** is the Smoke-free Environments Act 1990.

**5. Section 2 Amended (Interpretation)**

In section 2 of the principal act insert the following definitions alphabetically—

***

> **Commercial transaction** means any transaction or series of transactions—
>
> > a) with a body corporate, 
>
> > b) with a person whom it is reasonable to suspect is acting on behalf of a body corporate,
>
> > c) where tobacco products worth more than $500 are transacted, or 
>
> > d) where tobacco manufacturing equipment or intellectual property related to the same is transacted.
>
> **Due Diligence** means a requirement—
>
> > a) to monitor sales to their customers to ensure quantities are commensurate with legitimate demand.
>
> > b) to report to the police or the ministry of health any evidence that their customers are engaging in activities in contravention of obligations arising from the Protocol.
>
> **Illicit trade** means any practice or conduct prohibited by the law and that relates to production, shipment, receipt, possession, distribution, sale or purchase, including any practice or conduct intended to facilitate such activity under this act and others.
>
> **Manufacturing equipment** means machinery that is designed, or adapted, to be used solely for the manufacture of tobacco products and is integral to the manufacturing process.
>
> **Protocol** means the Protocol to Eliminate Illicit Trade in Tobacco Products.
>
> **Tobacco products** means products entirely or partly made of the leaf tobacco as raw material, which are manufactured to be used for smoking, sucking, chewing or snuffing.
>
> **Tracking and tracing** means systematic monitoring and re-creation by competent authorities or any other person acting on their behalf of the route or movement taken by items through the supply chain, as outlined in Article 8 of the Protocol.
>
> **Traditional small-scale growers, farmers and producers** means a person accredited under section 53.
>
> **Supply chain** means the range of activities and processes that covers the—
> 
> > a) manufacture of tobacco products and manufacturing equipment, 
>
> > b) import or export of tobacco products and manufacturing equipment, 
>
> > c) retailing of tobacco products,
>
> > d) transporting commercial quantities of tobacco products or manufacturing equipment,
>
> > e) wholesaling, brokering, warehousing or distribution of tobacco and tobacco products or manufacturing equipment, and
>
> > f) growing of tobacco not including that done by traditional small-scale growers, farmers and producers.

***

**6. New Part 4 Inserted - Ratification And Connected Duties Of Ministers**

After Part 3 of the Principle Act Insert—

***

> ## Part 4 - Ratification Of The Protocol to Eliminate The Trade In Illicit Tobacco
>
> **43. Duty Of Ministers To Seek Ratification**
>
> 1) It is the duty of the Minister Of Foreign Affairs to take all reasonable steps to enable New Zealand to ratify the protocol within three months of this act passing.
>
> 2) If the Protocol is not ratified at the time required by (1) the Minister has a duty to make a statement to parliament updating the house on the progress of ratification.
>
> 3) The subsection (2) duty applies every 6 months after the first statement until ratification is achieved.
>
> **44. Duty Of Ministers Generally**
>
> 1) When making decisions and directions, ministers must give consideration to the objective of eliminating the trade in illicit tobacco.
>
> 2) Ministers further have a positive duty to enable—
>
> > a) information sharing on the details of seizures, illicit activity uncovered, trends observed and concealment methods uncovered between those parties to the protocol and competent international authorities as deemed appropriate by the minister.
> > 
> > b) information sharing with other countries to assist with enforcement respecting confidentiality and privacy cooperation and assistance between countries regarding law enforcement, investigation and prosecution of offences, and technical matters in relation to the illicit trade in tobacco products.
> > 
> > c) providing mutual legal and administrative assistance to other members of the protocol where appropriate and lawful.

***

**7. New Part 5 Inserted - Licence, equivalent approval or control system**

After Part 4 of the Principle Act Insert—

***

> ## Part 5 - Licences, equivalent approval and the duties of licensees 
>
> **45. Licences**
>
> 1) The Minister may grant a body corporate may grant upon application a Tobacco Industry Operating License provided they find the body to be fit to carry out the duties attached to the license.
>
> 2) The Licence may grant the body corporate the ability to conduct business in whole or in part of the tobacco products supply chain.
>
> 3) By applying for a Tobacco Industry Operating License, the body corporate agrees to be bound by the requirements of Licensees under section 47, 48 and 49.
>
> 4) The Minister may charge no fee for the application in excess other than that which covers administrative costs.
>
> **46. Equivalent Approval**
>
> 1) The Minister may from time to time publish in the gazette a list of recognised equivalent licenses. 
>
> 2) The Minister must only recognise license schemes which meet the minimum requirements of the protocol.
>
> **47. Duty Of Licensees To Keep Records**
>
> 1) It is a duty of licensees to maintain records of all commercial transactions relevant to activities in the tobacco industry supply chain for a period of four years after the transaction.
>
> 2) If the business partner in a transaction was a traditional small-scale growers, farmers or producer this does not in any way remove a duty under this section.
>
> **48. Duty Of Licensees To Due Diligence**
>
> 1) Licensees have a duty to conduct due diligence before the commencement of and during the course of a business relationship.
>
> 2) If the business partner in a transaction was a traditional small-scale growers, farmers or producer this does not in any way remove a duty under this section.
>
> **49. Duty Of Licensees To Report Suspicious Activity**
>
> 1) It is a duty for a licensee to report any transaction involving the supply of tobacco products or tobacco manufacturing equipment  that it has made or proposes to make if the seller believes that the transaction is suspicious. 
>
> 2) A transaction is “suspicious” if there are reasonable grounds for suspecting that the substance in question is intended for any illicit use. 
>
> 3) In deciding whether there are reasonable grounds for suspecting illicit activity, the seller must consider if the prospective seller—
>
> > a) appears unclear about the intended use of the products, 
>
> > b) appears unclear about a stated intended use or is unable to satisfactorily explain it, 
>
> > c) intends to buy products in quantities uncommon for private use, 
>
> > d) is unwilling to provide proof of identity or place of residence if asked, or 
>
> > e) insists on using unusual methods of payment.
>
> **50. Miscellaneous Duties**
>
> 1) Licences have a duty to pay the Tobacco products levy as set out in Part 8.
>
> 2) Licences have a duty to meet the requirements of Part 7 - Track And Trace Of Tobacco Products on all Tobacco products.

***

**9. New Part 6 Inserted - Offences In Relation To A License**

After Part 5 of the Principle Act Insert—

***

> ## Part 6 - Offences In Relation To A License
>
> **51. Prohibition On Trading In The Tobacco Supply Chain Without A License**
>
> 1) It is an offence to conduct business in the Tobacco products supply chain without a Tobacco Industry Operating License granted under section 45.
>
> 2) Traditional small-scale growers, farmers or producer are exempt from this section in respect to trading in the growing and sale of raw leaf tobacco only.
>
> 3) A person guilty of an offence under this section is liable— 
> 
> > a) on summary conviction, to — 
> > 
> > > i) imprisonment for a term not exceeding 6 months; or 
> > > 
> > > ii) a fine not exceeding 2 times the value of the goods to which the offence relates; or 
> > > 
> > > iii) both 
> > 
> > b) on conviction on indictment, to 
> > 
> > > i) imprisonment for a term not exceeding one year; or 
> > > 
> > > ii) a fine not exceeding 2 times the value of the goods to which the offence relates; or 
> > > 
> > > iii) both 
>
> 4) A body corporate guilty of an offence under this section is liable to a fine not exceeding a value greater than — 
> 
> > a) 2 times the value of the goods to which the offence relates, or
> > 
> > b) 5% of the body corporate's annual turnover.
>
> Whichever is greater
>
> 5) For a body corporate to be guilty of an offence under this section it is sufficient that an offence under subsection (1), have been committed by an individual in the course of their duties for the body corporate.
>
> 6) For the purposes of this section a product in the tobacco industry that is sold  
> 
> **52. Offence Of Failing To Carry Out The Duties Of A Licensee**
>
> 1) It is an offence for a licensee to cause duties set out in sections 47, 48 and 49 to fail to be met.
> 
> 2) A person guilty of an offence under this section is liable to a fine not exceeding $2000.
>
> 3) A body corporate guilty of an offence under this section is liable to a fine not exceeding a value greater than — 
> 
> > a) 5 times the value of the goods to which the offence relates, or
> > 
> > b) 1% of the body corporate's annual turnover.
>
> Whichever is greater
>
> 4) For a body corporate to be guilty of an offence under this section it is sufficient that an offence under subsection (1), the offence must have been committed by an individual in the course of their duties for the body corporate.
>
> **53. Traditional small-scale growers, farmers or producer**
>
> 1) The Minister may by regulations and a notice in the gazette amend this section.
>
> 2) A Small-scale grower, farmer or producer means an individual growing tobacco leaf in an area of less than one hectare.

***

**10. New Part 7 Inserted - Track And Trace Of Tobacco Products**

After Part 7 of the Principle Act Insert:

***

> ## Part 7 - Track And Trace Of Tobacco Products
>
> **54. Secure Markings**
>
> 1) All Tobacco Products manufactured, imported, exported or sold in New Zealand must have a data matrix marking affixed to or form part of all unit packets and packages and any outside packaging.
>
> 2) The data matrix must link to the central website.
>
> 3) The link at the central website must contain—
>
> > a) the date and location of manufacture,
>
> > b) the manufacturing facility, 
>
> > c) a product description, 
>
> > d) the intended market of retail sale,
>
> > e) information that allows for the determination of the—
>
> > > i) machine, and
>
> > > ii) the production shift or time
>
> > of manufacture,
>
> > f) the name, invoice, order number and payment records of the first customer not affiliated with the manufacturer,
>
> > g)  the identity of any known subsequent purchaser,
>
> > h) the intended shipment route, 
>
> > i) shipping date, 
>
> > j) destination, 
>
> > k) point of departure, and 
>
> > l) consignee  
>
> Which each appropriate information being added to the link at the time of production, first shipment or import.
>
> 4) The Minister may my regulations create or amend requirements of the marking.
>
> **55. Central Website And Global Focal Point**
>
> 1) The Minister shall maintain a central website which licensee may enter information into.
>
> 2) Appropriate foreign or international authorities may request that the minister grant them access to information on the central website.
>
> 3) Access may only be granted to specific information records.
>
> 4) Access may only be granted for the purposes of a criminal investigation.
>
> **56. Inspectors**
>
> The Minister Of Health may employ and give remunerations to Inspectors for the purposes of ensuring compliance with the conditions of Tobacco Industry Licenses.
>
> **57. Powers Of Inspectors**
>
> 1) An Inspector may enter any premises or facility that is part of the tobacco supply train or which they reasonably believe contains information related to the tobacco supply train.
>
> 2) An Inspector may require any person who holds relevant information to give the Inspector— 
>
> > a) any of the information, 
>
> > b) a copy of any of the information, 
>
> > c) an explanation of any of the information, or 
>
> > d) access to any of the information. 
>
> 3) But the Inspector may not require the disclosure of information under this section if the information— 
> 
> > a) might incriminate the person disclosing it, or
>
> > b) is an item subject to legal privilege.
>
> 5) A disclosure of information to the Inspector in accordance with this section does not breach— 
> 
> > a) any obligation of confidence owed by any person, or (b) any other restriction on the disclosure of information other than restrictions imposed by the Privacy Act 1993.

***

**10. New Part 8 Inserted - Tobacco Products Levy**

After Part 7 of the Principle Act Insert—

***

> ## Part 8 - Tobacco Products Levy
>
> **58. Tobacco Products Levy**
>
> 1) A levy of 0.1% shall be applied to all sales of tobacco products.
>
> 2) A levy of 1% shall be applied to all sales of tobacco manufacturing equipment.
>
> 3) The proceeds of the levies shall cover the costs of inspections, audits, licensing and administration borne by the Ministry Of Health under this act.
>
> 4) Any revenue generated by the License in excess to those costs shall be given to lung disease charities.
>
> 5) The Minister may by regulations change the rate of the levy.
>
> 6) In setting the rate of the levy the Minister must aim to break even, allowing for some safety based upon variation in forecast future revenue. 

***

**11. Section 28 amended (Free distribution and rewards prohibited)**

1) Section 28(1) is amended by omitting “or retailer” and inserting “retailer or wholesaler”.

2) Section 28(1)(c) is amended by inserting “or wholesaler” after “retailer”, and by inserting “or wholesalers” after “retailers”.
