---
title: Land Transport (Autonomous Vehicle) Amendment Act 2019
bill: 216
introducer: Gregor_The_Beggar
author: 
- ItsKittay (Social Credit)
- FinePorpoise (Independent)
party: ACT
portfolio: Infrastructure
assent: 2019-11-23
commencement: 2019-11-24 (Section 5 by OiC)
first_reading: reddit.com/r/ModelNZParliament/comments/drux31/b216_land_transport_autonomous_vehicle_amendment/
committee: reddit.com/r/ModelNZParliament/comments/duow7k/b216_land_transport_autonomous_vehicle_amendment/
final_reading: reddit.com/r/ModelNZParliament/comments/dxiyg2/b216_land_transport_autonomous_vehicle_amendment/
amends:
- Land Transport Act 1998
---

# Land Transport (Autonomous Vehicle) Amendment Act 2019

**1. Title**

This Act may be cited as the Land Transport (Autonomous Vehicles) Amendment Act 2019.

**2. Commencement**

1) This Act comes into force on the day after the date it receives Royal Assent.

2) Section 5 comes into force on the day appointed by the Governor-General by Order in Council, after an assessment model is established under section 6. 

**3. Purpose**

The purpose of this Act is to remove legal doubt that autonomous vehicles may be operated in New Zealand.

**4. Interpretation**

The **principal Act** is the Land Transport Act 1998.

**5. New section 9A inserted**

Following section 9, insert a new section 9A:

***

> **9A. No requirement for human driver**
>
> For the avoidance of doubt, there is no requirement for a vehicle to have a human driver.

***

**6. Minister must establish assessment model**

1) The Minister must establish an assessment model for driverless vehicles to be certified for entry onto the road.

2) Assessments made under the model established under subsection (1) must be publically available.

3) A person may not operate a driverless vehicle unless the driverless vehicle has been certified under the assessment model established under subsection (1).

**7. New Part 10A inserted (Liability for collisions involving driverless vehicles)**

1) After section 151, insert the following new Part 10A:

***

> ## Part 10A: Liability for collisions involving driverless vehicles
> 
> **151A. Liability for collisions involving driverless vehicles**
> 
> 1)  This section applies where a person has suffered loss resulting from a collision caused by a vehicle in circumstances where, if an individual had been driving the vehicle, that individual would be liable for the loss and—
> 
> > a) the cause of the collision was related to—
> 
> > > i) the way the vehicle moved or failed to move;
> 
> > > ii) the way the vehicle was positioned;
> 
> > > iii) the failure of the vehicle to have its headlamps or tail lamps on in lighting conditions under which they should have been on;
> 
> > > iv) the failure of the vehicle to dip its headlamps; or
> 
> > > v) the way that the vehicle signalled or failed to signal; and
> 
> > b) the vehicle was driving itself at the time of the collision using an unsupervised autonomous mode of operation—
> 
> > > i) within the circumstances under which the manufacturer has indicated the mode is capable of operating without supervision by an individual driver; or
> 
> > > ii) outside those circumstances and no individual who has operated the vehicle can be shown to be responsible for this.
> 
> 2) This section does not apply if any of the circumstances outlined in subsection (1)(a) were caused by inadequate maintenance or damage to the vehicle.
> 
> 3) Where this section applies any person who has suffered loss may obtain damages from the manufacturer of the vehicle.
> 
> 4) For the avoidance of doubt, section 177A of the Crimes Act 1996 is applicable where relevant.

***

2) In section 2(1), insert the following new definition in the appropriate alphabetical order:

***

> **unsupervised autonomous mode of operation** means a mode of operation of a vehicle in relation to which the manufacturer has indicated that, provided the vehicle is not directed to operate outside the circumstances under which the manufacturer has indicated the mode is capable of operating without supervision by an individual driver, no supervision by an individual is required.

***
