---
title: Resource Management (Emissions Exemptions) Act 2019
bill: 223
introducer: imnofox
author: 
- imnofox (Greens)
party: Greens
portfolio: Member's Bills
assent: 2019-12-06
commencement: 2019-12-07
first_reading: reddit.com/r/ModelNZParliament/comments/dxizxb/b223_resource_management_emissions_exemptions/
committee: reddit.com/r/ModelNZParliament/comments/e0ee1g/b223_resource_management_emissions_exemptions/
final_reading: reddit.com/r/ModelNZParliament/comments/e3ab9y/b223_resource_management_emissions_exemptions/
amends:
- Resource Management Act 1991
- Exclusive Economic Zone and Continental Shelf (Environmental Effects) Act 2012
---

# Resource Management (Emissions Exemptions) Act 2019

**1. Title**

This Act is the Resource Management (Emissions Exemptions) Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this Act is to remove clauses that rule out the ability of councils and consent authorities to consider greenhouse gas emissions when making rules and when considering a resource, to remove clauses that rule out the ability of the EPA to consider greenhouse gas emissions when deciding applications for marine consents, and to place emission reductions under the Minister's considerations when making regulations under the EEZ Act.

## Part 1: Amendments to Resource Management Act 1991

**4. Principal Act amended**

This Part amends the Resource Management Act 1991.

**5. Section 70A repealed (Application to climate change of rules relating to discharge of greenhouse gases)**

Section 70A is repealed.

**6. Section 70B amended (Implementation of regulations made under section 43)**

Section 70B is amended by omitting “, provided the rules are no more or less restrictive than the regulations”.

**7. Section 104E repealed (Applications relating to discharge of greenhouse gases)**

Section 104E is repealed.

**8. Section 104F amended (Implementation of regulations made under section 43)**

Section 104F is amended by omitting from paragraph (a) “; but”, and by repealing paragraph (b).

## Part 2: Amendments to Exclusive Economic Zone and Continental Shelf (Environmental Effects) Act 2012

**9. Principal Act amended**

This Part amends the Exclusive Economic Zone and Continental Shelf (Environmental Effects) Act 2012.

**10. Section 11 amended (International obligations)**

After section 11(d), insert:

***

> e) the United Nations Framework Convention on Climate Change and protocols to and agreements under that convention.

***

**11. Section 33 amended (Matters to be considered for regulations under section 27)**

After section 33(3)(f), insert:

***

> fa) the importance of reducing discharges into the air of greenhouse gases; and

***

**12. Section 59 amended (Marine consent authority’s consideration of application)**

Section 59 is amended by deleting paragraph (5)(b).
