---
title: Resource Management (Production Land Exemptions) Amendment Act 2019
bill: 231
introducer: imnofox
author: 
- imnofox (Greens)
party: Greens
portfolio: Finance
assent: 2019-12-24
commencement: 2021-04-01
first_reading: reddit.com/r/ModelNZParliament/comments/e1u5pi/b231_resource_management_production_land/
committee: reddit.com/r/ModelNZParliament/comments/e4v10b/b231_resource_management_production_land/
final_reading: reddit.com/r/ModelNZParliament/comments/eca7bf/b231_resource_management_production_land/
amends:
- Resource Management Act 1991
---

# Resource Management (Production Land Exemptions) Amendment Act 2019

**1. Title**

This Act is the Resource Management (Production Land Exemptions) Amendment Act 2019.

**2. Commencement**

This Act shall come into force on 1 April 2021.

**3. Purpose**

The purpose of this Act is to amend the Resource Management Act to remove distortions by repealing unique resource consenting exemptions for production land.

**4. Principal Act Amended**

This Act amends the Resource Management Act 1991.

**5. Section 2 amended (Interpretation)**

1) Section 2(1) is amended by repealing the definition of "production land".

2) The definition of industrial or trade premises in section 2(1) is amended by omitting the words "but does not include any production land".

**6. Transitional provisions**

Insert the following new section 383B:

***

> **383B. Right of persons to discharge contaminants from, onto, or into production land**
>
> 1) Where a person is discharging contaminants into the air from production land immediately before the date of commencement of this Act, they shall be deemed to have a discharge permit granted under this Act until 1 April 2025.
> 
> 2) Where a person is discharging contaminants onto or into land from production land immediately before the date of commencement of this Act, they shall be deemed to have a discharge permit granted under this Act until 1 April 2023.

***
