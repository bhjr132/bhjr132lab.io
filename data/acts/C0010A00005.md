---
title: Electoral (Expansion of Franchise) Amendment Act 2020
bill: 240
introducer: MerrilyPutrid
author:
- KatieIsSomethingSad (Labour)
- forgottomentionpeter (Greens)
- Golriz Ghahraman (IRL figure)
party: Greens
portfolio: Justice
assent: 2020-02-17
commencement: 2020-02-18
first_reading: reddit.com/r/ModelNZParliament/comments/evhvy1/b240_electoral_expansion_of_franchise_amendment/
committee: reddit.com/r/ModelNZParliament/comments/eyirag/b240_electoral_expansion_of_franchise_amendment/
final_reading: reddit.com/r/ModelNZParliament/comments/f1ou8s/b240_electoral_expansion_of_franchise_amendment/
---

# Electoral (Expansion of Franchise) Amendment Act 2020

**1. Title**

This Act is the Electoral (Expansion of Franchise) Amendment Act 2020.

**2. Purpose**

The purpose of this Act is to reduce the age of eligibility to vote from 18 years to 16 years.

**3. Commencement**

This Act comes into force the day after it receives Royal Assent.

**4. Principal Act**

This Act amends the Electoral Act 1993 (the **principal Act**).

**5. Section 3 amended (Interpretation)**

1) In section 3, definition of **adult**,

> a) paragraphs (a) and (b), replace “18 years” with “16 years”.
> 
> b) paragraph (b), strike the word “18th” and insert in its place “16th”.

2) In section 3, definition of **Māori electoral population**, replace “18 years” with “16 years”.

**6. Section 60 amended (Who may vote)**

In section 60(f), replace “18 years” with “16 years”.

**7. Section 82 amended (Compulsory registration of electors)**

In section 82(2),

> 1) replace “17 years” with “15 years”; and
> 
> 2) replace “18 years” with “16 years” in both places.

**8. Section 89D amended (Inquiry to be made to update electoral rolls)**

In section 89D(7)(a), replace “17 years” with “15 years”.

**9. Section 92 amended (Notification of death of registered elector)**

In section 92(1), replace “17 years” with “15 years”.

**10. Section 263B amended (Disclosure of personal information for enrolment purposes)**

In section 263B(2), replace “17 years” with “15 years”.

**11. Section 268 amended**

In section 268(1)(e), replace “18 years” with “16 years”.
