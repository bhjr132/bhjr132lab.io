---
title: Electoral (Strengthening Democracy) Amendment Act 2020
bill: 241
introducer: MerrilyPutrid
author:
- forgottomentionpeter (Greens)
- Golriz Ghahraman (IRL figure)
- Andrew Little (IRL figure)
party: Greens
portfolio: Justice
assent: 2020-02-17
commencement: 2020-02-18
first_reading: reddit.com/r/ModelNZParliament/comments/evhwy9/b241_electoral_strengthening_democracy_amendment/
committee: reddit.com/r/ModelNZParliament/comments/eyirbv/b241_electoral_strengthening_democracy_amendment/
final_reading: reddit.com/r/ModelNZParliament/comments/f1ouea/b241_electoral_strengthening_democracy_amendment/
urls:
- https://docs.google.com/document/d/1r0bOjep9_TOU1dMbbl7HSNgLH-VrxCnjkIbznbYK78g/edit
---
