---
title: Education (Tertiary Pastoral Care) Amendment Act 2020
bill: 267
introducer: Captain_Plat_2258
author:
- Hon. Chris Hipkins (IRL figure)
party: Greens
portfolio: Education
assent: 2020-05-24
commencement: 2020-05-25
first_reading: reddit.com/r/ModelNZParliament/comments/fwfmu3/b267_education_tertiary_pastoral_care_amendment/
committee: reddit.com/r/ModelNZParliament/comments/ga3zvw/b267_education_tertiary_pastoral_care_amendment/
final_reading: reddit.com/r/ModelNZParliament/comments/gds98p/b267_education_tertiary_pastoral_care_amendment/
urls:
- https://drive.google.com/file/d/1vM7q5HRhZqI4TrCcLpk_Yfnbg1k2lYdS/view
amends:
- Education Act 1989
---

# Education (Tertiary Pastoral Care) Amendment Act 2020

**1. Title**

This Act is the Education (Tertiary Pastoral Care) Amendment Act 2020.

**2. Commencement**

This Act comes into force on the day after the date of Royal assent.

**3. Principal Act**

This Act amends the Education Act 1989 (the **principal Act**).

**4. Purpose**

The purpose of this Act is to amend the Education Act 1989 to enable the Minister of Education to
issue a mandatory code of practice applying to the pastoral care of domestic tertiary students, in
addition to or in conjunction with the pastoral care code which currently applies to international
tertiary students.

**5. Part 18A replaced**

Replace Part 18A with:

*** 

> ## Part 18A - Pastoral care of students
> 
> **238D. Interpretation**
> 
> In this Part, unless the context otherwise requires,—
> 
> **code** means a code of practice issued under section 238G
> 
> **code administrator** means the person or agency appointed under section 238H(1)
> 
> **domestic tertiary student** means an individual—
> 
> > a) who is—
> > 
> > > i) a New Zealand citizen; or
> > > 
> > > ii) the holder of a residence class visa granted under the Immigration Act 2009 who satisfies the
criteria (if any) prescribed by regulations made under section 2(4); or
> > > 
> > > iii) a person of a class or description of persons required by the Minister, by notice in the Gazette, to
be treated as if they are not international students; and
> > 
> > b) who is a tertiary student enrolled at an institution or a registered establishment
> 
> **DRS operator** means a person or an agency appointed under section 238M(4)(a)
> 
> **DRS rules** means the rules made under section 238P
> 
> **international student** means an individual who is not a domestic student
> 
> **provider** means a person or body that is,—
> 
> > a) in respect of international students, a registered school, an institution, or a registered
establishment; or
> > 
> > b) in respect of domestic tertiary students, an institution or a registered establishment
> 
> **serious harm**, in relation to a domestic tertiary student or an international student, means an event or circumstances that seriously and detrimentally affect the ongoing welfare of the student, including (but not limited to) physical injury, physical illness, or mental illness
> 
> **signatory provider** means a provider that is a signatory to a code issued under section 238G(1)(b) or (c)
> 
> **student accommodation** means premises that are exempt under section 5B of the Residential Tenancies Act 1986
> 
> **student claimant**, in relation to a provider or signatory provider, means a person who—
> 
> > a) is a domestic tertiary student or an international student enrolled by the provider or signatory provider; or
> > 
> > b) is a former domestic tertiary student or international student enrolled by the provider or signatory provider; or
> > 
> > c) intends to be, or is in the process of being, enrolled by the provider or signatory provider as a domestic tertiary student or an international student
> 
> **student contract dispute resolution scheme** or **DRS** means the student contract dispute resolution scheme established by section 238M.
> 
> ### *Enrolment of international students*
> 
> **238E. Signatory providers may enrol persons as international students**
> 
> 1) A provider may enrol a person as an international student or continue to have an international student enrolled, as long as the provider is a signatory provider.
> 
> 2) A provider may not enrol a person as an international student or continue to have an international student enrolled, or provide educational instruction for the person, if the provider—
> 
> > a) is not a signatory provider; or
> > 
> > b) is removed as a signatory provider under section 238L; or
> > 
> > c) ceases to be a signatory provider for any other reason provided in the code.
> > 
> 3) A provider or signatory provider that is subject to a sanction under section 238L(1) may continue to have international students enrolled and may provide educational instruction to those students, but only to the extent permitted by the sanction.
> 
> **238F. Providers must enrol persons as international students in certain circumstances**
> 
> A provider must enrol a person as an international student if the person is not a domestic student and
the provider—
> 
> > a) provides the person with educational instruction for more than 2 weeks; or
> > 
> > b) accepts tuition fees from the person.
> 
> ### *Pastoral care of domestic and international students*
> 
> **238G. Pastoral care codes of practice**
> 
> 1) The Minister may issue—
> 
> > a) a code that provides a framework for the pastoral care of domestic tertiary students:
> > 
> > b) a code that provides a framework for the pastoral care of international students:
> > 
> > c) a code that provides a framework for the pastoral care of domestic tertiary students and international students.
> 
> 2) The purpose of a code—
> 
> > a) in respect of domestic tertiary students is to support the Government’s objectives for the education of domestic tertiary students by—
> > 
> > > i) requiring providers to take all reasonable steps to maintain the well-being of domestic
tertiary students; and
> > > 
> > > ii) ensuring, so far as is possible, that domestic tertiary students have a positive experience that supports their educational achievement:
> > 
> > b) in respect of international students is to support the Government’s objectives for international
education by—
> > 
> > > i) requiring providers to take all reasonable steps to protect international students; and
> > >
> > > ii) ensuring, so far as is possible, that international students have in New Zealand a positive experience that supports their educational achievement.
> 
> 3) The scope of a code—
> 
> > a) in respect of domestic tertiary students is to prescribe, alongside other quality assurance
prescribed by this Act,—
> > 
> > > i) outcomes sought from providers for their domestic tertiary students; and
> > > 
> > > ii) key processes required of providers to support the well-being, achievement, and rights of domestic tertiary students:
> 
> > b) in respect of international students is to prescribe, alongside other quality assurance
prescribed by this Act,—
> > 
> > > i) outcomes sought from providers for their international students; and
> > > 
> > > ii) key processes required of providers to support the well-being, achievement, and rights of international students.
> 
> 4) Without limiting subsections (1) to (3), a code may include provisions for 1 or more of the following purposes:
> 
> > a) despite anything in the Public Finance Act 1989, requiring providers to indemnify the code administrator:
> > 
> > b) providing for any other matters contemplated by this Part, necessary for its administration, or
necessary for giving it full effect.
> 
> 5) Before issuing a code, the Minister must consult—
> 
> > a) those parties that the Minister considers likely to be affected by the code, including representatives of students, parents, providers, signatory providers, and the staff of providers and signatory providers; and
> > 
> > b) the Privacy Commissioner.
> 
> 6) A code—
> 
> > a) in respect of domestic tertiary students is binding on all providers:
> > 
> > b) in respect of international students is binding on all signatory providers.
> > 
> 7) A code—
> 
> > a) may make different provisions in relation to students under the age of 18 years and in relation to students aged 18 years or over:
> > 
> > b) must be published on an Internet site maintained by or on behalf of the Ministry:
> > 
> > c) is a disallowable instrument, but is not a legislative instrument, for the purposes of the Legislation Act 2012 and must be presented to the House of Representatives under section 41 of that Act.
> 
> **238H. Code administrators**
> 
> 1) The Minister may, by notice in the Gazette, appoint a person or an agency to be responsible for—
> 
> > a) administering—
> > 
> > > i) the code for domestic tertiary students:
> > > 
> > > ii) the code for international students:
> > > 
> > > iii) the code for domestic tertiary students and international students; and
> > 
> > b) administering 2 or more of the codes.
> 
> 2) When appointing a code administrator, the Minister must have regard to the knowledge, skills, or
experience of the person or agency.
> 
> 3) The functions of the code administrator are,—
> 
> > a) in the case of international students,—
> > 
> > > i) to receive applications from providers seeking to become signatory providers; and
> > > 
> > > ii) to assess those applications against—
> > > 
> > > > A) the criteria stated in a code; and
> > > > 
> > > > B) the purpose stated in section 238G(2) and the scope stated in section 238G(3); and
> > > 
> > > iii) to allow or decline to allow applicants to become signatory providers in accordance with
those assessments; and
> > 
> > b) in the case of domestic tertiary students and international students,—
> > 
> > > i) to monitor and investigate the extent to which providers comply with a code—
> > > 
> > > > A) following a process prescribed by a code; and
> > > > 
> > > > B) in close collaboration with education quality assurance authorities; and
> > > 
> > > ii) to issue to providers notices under section 238I or 238J; and
> > > 
> > > iii) to impose on providers sanctions for breaches of a code and failure to comply with
notices under section 238I or 238J.
> 
> 4) A code administrator may allow an applicant to become a signatory provider unconditionally, or subject to any written conditions made known to the applicant at the time the code administrator informs the applicant that the applicant is allowed to become a signatory provider.
> 
> 5) A code administrator may, in accordance with the requirements of the applicable code,—
> 
> > a) enter any student accommodation and inspect the premises and facilities:
> > 
> > b) inspect, and make and remove copies of, any information relating to the management of the student accommodation:
> > 
> > c) ask any person at any student accommodation to make or provide statements, in whatever form or manner is reasonable in the circumstances, about any matter relating to the safety of students who board at the student accommodation.
> 
> 6) A code administrator may exercise the powers in subsection (5) only for the purposes of monitoring or investigating compliance with the relevant code.
> 
> 7) A code administrator may not enter or inspect the room or sleeping area of a student accommodated at any student accommodation unless—
> 
> > a) the code administrator believes on reasonable grounds that the entry or inspection is necessary for the purposes specified in subsection (6); and
> > 
> > b) the code administrator gives the student at least 24 hours’ written notice of the intended entry or inspection that explains the purpose of the entry or inspection; and
> > 
> > c) the student consents to the entry or inspection, and the student is present during the entry or inspection if being present is a condition of the consent.
> 
> 8) Subsection (7)(c) does not apply if obtaining the student’s consent is unreasonable in the circumstances.
> 
> 9) A code administrator may, with the Minister’s prior written approval and subject to any conditions that the code administrator or the Minister thinks fit, delegate any or all of the functions, duties, and powers specified in this section to another person or agency (having regard to their knowledge, skills, or experience).
> 
> ### *Compliance*
> 
> **238I. Quality improvement notices**
> 
> 1) A code administrator may issue a quality improvement notice to a provider that requires the provider to do, or refrain from doing, a particular thing in relation to the provider’s obligations under a code.
> 
> 2) The notice must—
> 
> > a) set out any concerns the administrator has about the provider’s systems, practices, training, or
procedures; and
> > 
> > b) specify the time within which the provider is expected to address the administrator’s concerns (which must be a reasonable time, having regard to the nature and complexity of the action required); and
> > 
> > c) specify the possible consequences of a failure to comply with a quality improvement notice.
> 
> 3) The code administrator may publish the notice, or a summary of it, in a manner designed to give public notice of it.
> 
> 4) The code administrator may, before the expiry of the time or period referred to in subsection (2)(b), extend the time or period, and in that case the time or period as extended becomes the time or period within or during which the notice must be complied with.
> **238J. Compliance notices**
> 
> 1) A code administrator may issue a compliance notice to a signatory provider that requires the signatory provider to do, or refrain from doing, a particular thing in relation to—
> 
> > a) the signatory provider’s obligations as a signatory to a code; or
> > 
> > b) the signatory provider’s conditions of approval as a signatory to a code.
> 
> 2) A notice under subsection (1) may specify the manner in which a thing must be done or the manner in which the consequences of a thing must be rectified.
> 
> 3) The notice must be in writing and must—
> 
> > a) state the date on which it is issued; and
> > 
> > b) if it requires the signatory provider to take any action, state a time on or before which, or a period within which, the signatory provider must take the action; and
> > 
> > c) state the consequences or possible consequences of non-compliance with the notice.
> 
> 4) The code administrator may publish the notice, or a summary of it, in a manner designed to give public notice of it.
> 
> 5) The code administrator may, before the expiry of the time or period referred to in subsection (3)(b), extend the time or period, and in that case the time or period as extended becomes the time or period within or during which the notice must be complied with.
> 
> **238K. Providers and signatory providers to comply with notices**
> 
> 1) A provider must comply with a quality improvement notice issued under section 238I.
> 
> 2) A signatory provider must comply with a compliance notice issued under section 238J.
> 
> 3) If, in a code administrator’s opinion, a provider or signatory provider does not comply with the relevant notice, the code administrator may (as the code administrator sees fit) impose sanctions against the provider or signatory provider under section 238L.
> 
> 4) The code administrator may not act under subsection (3) until the later of the following:
> 
> > a) 10 working days after the date on which the quality improvement notice or compliance notice
was issued:
> > 
> > b) the expiry of any time or period of a kind referred to in section 238I(2)(b) or 238J(3)(b).
> 
> 5) This section and sections 238I and 238J do not limit or affect section 238L.
> 
> **238L. Sanctions for breach of code**
> 
> 1) If satisfied that a provider or signatory provider has breached the relevant code or failed to comply with a notice under section 238I or 238J, a code administrator may,—
> 
> > a) in the case of signatory providers, impose new, or amend or revoke any existing, conditions on the signatory provider’s approval as a signatory provider:
> > b) in the case of providers and signatory providers, impose limitations on the provider’s or the signatory provider’s power to enrol students.
> 
> 2) If satisfied that a signatory provider has not complied with a sanction imposed under subsection (1) or a notice under section 238I or 238J, a code administrator may remove the signatory provider as a signatory provider.
> 
> ### *Dispute resolution*
> 
> **238M. Student contract dispute resolution scheme established**
> 
> 1) This section establishes a student contract dispute resolution scheme (the DRS).
> 
> 2) The purpose of the DRS is to resolve contractual and financial disputes between students (and former and prospective students) and providers or signatory providers.
> 
> 3) Every provider or signatory provider that enrols, intends to enrol, or has enrolled students is subject to and must (in relation to any contractual or financial dispute with a student or a former or prospective student) comply with the rules of the DRS prescribed under section 238P.
> 
> 4) The Minister—
> 
> > a) may, by notice in the Gazette, appoint 1 or more persons or agencies to be responsible for administering the DRS; and
> > 
> > b) must take reasonable steps to ensure that there is at all times a person or an agency appointed to be responsible for administering the DRS; and
> > 
> > c) may impose any conditions on the appointment that the Minister thinks fit.
> 
> 5) A student claimant may lodge with the DRS operator for resolution under the DRS any contractual or financial dispute with a provider or signatory provider, but only if—
> 
> > a) the provider or signatory provider has been given an opportunity to resolve the dispute, but the student claimant is not satisfied with the process, or outcome, or both; or
> > 
> > b) the provider or signatory provider—
> > 
> > > i) has not tried to resolve the dispute; or
> > > 
> > > ii) has refused to try to do so.
> 
> 6) However, a student may not lodge a dispute for resolution under subsection (5) later than 7 years after the date of the act or omission on which the dispute is based.
> 
> 7) The resolution of a dispute is binding on all parties to the dispute if—
> 
> > a) it is the outcome of an adjudication of the dispute by or on behalf of the DRS operator; or
> > 
> > b) it is produced by a mediation undertaken by or on behalf of the DRS operator whose outcome the parties to the dispute have agreed will be binding.
>
> 8) The DRS operator—
> 
> > a) may charge fees to a student claimant or the provider concerned, or both, according to the rules of the DRS prescribed under section 238P; but
> > 
> > b) (despite those rules) may in its absolute discretion partially or completely exempt any person, or persons of a particular description, from the payment of fees.
> 
> **238N. Cap on amount to be paid**
> 
> In resolving any dispute, the DRS operator—
> 
> > a) may not require a provider or signatory provider to pay to a student claimant in relation to any particular claim any amount exceeding $200,000; but
> > 
> > b) may charge the provider fees in addition to any amount required to be paid.
> 
> **238O. District Court to enforce DRS**
> 
> 1) The District Court may,—
> 
> > a) on the application of a student claimant or the DRS operator, make an order requiring a provider or signatory provider to comply with the rules of the DRS or to give effect to any resolution that is binding under section 238M(7); or
> > 
> > b) on the application of the provider or signatory provider or the DRS operator, make an order requiring a student claimant to give effect to any resolution that is binding under section 238M(7).
> 
> 2) If an order (or part of an order) requiring the provider or signatory provider to comply with the resolution requires the provider or signatory provider to pay any sum of money to any person, that order (or part) may be enforced as if it were a judgment by the District Court for the payment of that sum of money to that person.
> 
> 3) If the District Court is satisfied that the terms of the resolution of a dispute by the DRS operator are manifestly unreasonable, the court may modify the resolution before giving effect to it.
> 
> 4) Subsection (3) overrides subsections (1) and (2) and section 238M(7).
> 
> **238P. Rules of student contract dispute resolution scheme**
> 
> 1) The Governor-General may, by Order in Council made on the recommendation of the Minister, make rules for the functioning and administration of the DRS.
> 
> 2) Before recommending that an order be made, the Minister must consult any relevant bodies and sector representatives that the Minister thinks appropriate.
> 
> 3) The rules may prescribe fees, or a means by which fees can be calculated or ascertained.
> 
> ### *Export education levy*
> 
> **238Q. Export education levy**
> 
> 1) The Governor-General may, by Order in Council made on the recommendation of the Minister, make regulations imposing an export education levy on signatory providers who receive tuition fees from international students enrolled with them.
> 
> 2) Without limiting subsection (1), regulations made under this section must—
> 
> > a) prescribe the amount or the method for calculating the amount (or both) of export education levy payable by individual signatory providers, and may prescribe different amounts, or different methods of calculating the amounts, payable by different classes of signatory provider; and
> > 
> > b) prescribe when the levy, or any part of the levy, is payable; and
> > 
> > c) designate an agency to administer the levy and, if that agency is the Ministry, the regulations may authorise the Ministry to delegate all or specified aspects of the levy’s collection and use to another body; and
> > 
> > d) require a signatory provider to supply, on request by the agency responsible for the administration of the levy, information about student numbers or any other matter that is necessary to determine or verify the amount of levy payable by the signatory provider.
> 
> 3) Before recommending that an order be made, the Minister must consult signatory providers.
> 
> **238R. How export education levy may be applied**
> 
> 1) Export education levy funds may be applied for the following purposes:
> 
> > a) the development, promotion, and quality assurance of the export education sector, which may include (without limitation)—
> > 
> > > i) professional and institutional development; and
> > > 
> > > ii) marketing; and
> > > 
> > > iii) implementation of scholarship schemes; and
> > > 
> > > iv) research, and resource development; and
> > > 
> > > v) support (financial or otherwise) of other bodies engaged in the development, promotion, or quality assurance of the export education sector:
> > 
> > b) the making of payments as set out in subsections (2) and (3):
> > 
> > c) the administration and audit of the code in respect of international students:
> > 
> > d) the funding of the cost of the operation of the DRS established by section 238M that is attributable to international students:
> > 
> > e) the general administration of the levy and associated purposes.
> 
> 2) Subsection (3) applies if—
> > 
> > a) an international student is or was enrolled with a private training establishment or a private school for a course of study or training; and
> > 
> > b) at the time of the international student’s enrolment, the private training establishment held a current registration under Part 18, or the private school held a current registration under section 35A; and
> > 
> > c) the private training establishment or private school has not, cannot, or will not provide, in whole or in part, the course of study or training.
> 
> 3) If this subsection applies, the levy funds may be used for any of the following purposes:
> > 
> > a) to make payment to any person to ensure the reimbursement of the student, in whole or in part, for tuition fees or for any payment other than tuition fees made by or on behalf of that student to the private training establishment or private school in respect of the student’s course of study or training if, and to the extent that,—
> > 
> > > i) the private training establishment or private school has not refunded the tuition fees or other payment; and
> > > 
> > > ii) the agency responsible for the administration of the levy approves the reimbursement of the student as necessary and appropriate in the circumstances:
> > 
> > b) with the approval of the Minister, to reimburse the Crown for any sum provided by the Crown and paid to any person to ensure the reimbursement of the student, in whole or in part, for tuition fees or for any other payment made by or on behalf of that student to the private training establishment or private school in respect of the student’s course of study or training if, and to the extent that,—
> > 
> > > i) the private training establishment or private school had not refunded the tuition fees or other payment; and
> > > 
> > > ii) the agency responsible for the administration of the levy approved the reimbursement of the student as necessary and appropriate in the circumstances:
> > 
> > c) with the approval of the Minister, to reimburse, in whole or in part, the agency responsible for the administration of the levy, or any Crown entity, for—
> > 
> > > i) costs incurred by that agency or Crown entity in placing the student with an alternative provider; or
> > > 
> > > ii) other costs incurred by that agency or Crown entity as a direct result of the private training establishment or private school not providing the course of study or training.
> 
> 4) Levy funds must be kept in a separate bank account that is used only for the purposes of the levy.
> 
> 5) As soon as practicable after 1 July in each year, the agency responsible for the administration of the levy must present to the Minister an annual report on the administration of the levy, which must include audited financial statements prepared in accordance with generally accepted accounting practice, and the Minister must present a copy of the report to the House of Representatives.
> 
> 6) The amount of levy payable by a provider under regulations made under this section is a debt due to the Crown and may be recovered in any court of competent jurisdiction.
> 
> ### *Offences and penalties*
> 
> **238S. Offence relating to breach of code resulting in serious harm to or death of students**
> 
> 1) A provider or signatory provider commits an offence if—
> 
> > a) the provider or signatory provider, without reasonable excuse, breaches the applicable code; and
> > 
> > b) the breach results in serious harm to or the death of 1 or more of its students.
> 
> 2) A provider who commits an offence against this section is liable on conviction to a fine not exceeding $100,000.
> 
> **238T. Pecuniary penalty relating to breach of code**
> 
> 1) On the application of a code administrator, a court may order a provider or signatory provider to pay to the Crown any pecuniary penalty that the court determines to be appropriate if the court is satisfied that the provider or signatory provider has, without reasonable excuse, committed a serious breach of the applicable code.
> 
> 2) The amount of any pecuniary penalty may not exceed $100,000.
> 
> **238U. Relationship between offences and penalties**
> 
> 1) Once criminal proceedings against a provider or signatory provider for an offence under section 238S are determined, a court may not order the provider or signatory provider to pay a pecuniary penalty under section 238T in respect of the conduct, events, transactions, or other matters that were the subject of the criminal proceedings.
> 
> 2) Once civil proceedings against a provider or signatory provider for a pecuniary penalty under section 238T are determined, the provider or signatory provider may not be convicted of an offence under section 238S in respect of the conduct, events, transactions, or other matters that were the subject of the civil proceedings.
> 
> 3) Any uncompleted proceedings for an order under this Act that a provider or signatory provider pay a pecuniary penalty must be stayed if criminal proceedings are started or have already been started against the provider or signatory provider for the same act or omission, or substantially the same act or omission, in respect of which the pecuniary penalty order is sought.

***
