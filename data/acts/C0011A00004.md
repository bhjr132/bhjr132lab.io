---
title: Zero Carbon (Effectiveness of Provisions) Amendment Act 2020
bill: 269
introducer: Captain_Plat_2258
author:
- Captain_Plat_2258 (Greens)
party: Greens
portfolio: Environment
assent: 2020-05-24
commencement: 2020-05-25
first_reading: reddit.com/r/ModelNZParliament/comments/ga40xo/b269_zero_carbon_effectiveness_of_provisions/
committee: reddit.com/r/ModelNZParliament/comments/gds9a9/b269_zero_carbon_effectiveness_of_provisions/
final_reading: reddit.com/r/ModelNZParliament/comments/ghi2gr/b269_zero_carbon_effectiveness_of_provisions/
amends:
- C0001A00001
---

# Zero Carbon (Effectiveness of Provisions) Amendment Act 2020

**Purpose**

The purpose of this Act is to bring forward the end-goal date of the original Act from 2050 to 2035, to expand the powers of the Independent Climate Commission, and put in place more strict and comprehensive provisions for the reduction of emissions to account for the earlier end-goal date.

**1. Title**

This Act is the Zero Carbon (Effectiveness) Amendment Act 2020.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Zero Carbon Act 2017 (the **principal Act**).

**4. Section 3 amended (Zero Carbon target)**

1) In subsection (1), replace “2050” with “2035”

2) In subsection (2), replace “2050” with “2035”

**5. Section 5 amended (Carbon budgets)**

1) in section (5) insert new subsection:

***

> 3) the Minister responsible for Climate Change must consult with and enact the advice of the Independent Climate Commission when setting the carbon budget.

***

**6. Section 6 amended (Government plan)**

1) in subsection (2), replace “the government’s plan to meet carbon budgets” with “the government’s plan to meet carbon budgets; and”

2) In subsection (2), add the following paragraphs after “the government’s plan to meet carbon budgets; and”:

***

> d) the government’s plan to increase renewable energy sources and reduce national dependency on energy sources that contribute to emissions, with an end-goal target of full dependence on renewable energy sources by **2035**; and  
>  
> e) the government’s intentions around support via subsidies for research and development into renewable energy sources; and  
>  
> f) the government’s intended tax rates on carbon emission by businesses and corporations; and  
>  
> g) the government’s intentions around reducing and replacing the number of publicly and privately owned carbon emitting vehicles and other modes of transport, including busses and light/heavy rail, with non-emitting or low-emission alternatives, with an end-goal target to eliminate or heavily reduce the number of carbon emitting vehicles by **2035**; and  
>  
> h) the government’s intentions around collaboration with farmers to eliminate or heavily reduce carbon emissions from the agricultural sector; and  
>  
> i) the government’s intentions around collaboration with manufacturers to eliminate or heavily reduce carbon emissions from the industrial manufacturing sector; and  
>  
> j) the government’s plan to ensure anybody made redundant in relation to anti-emission measures and their effects will be kept out of unemployment wherever possible.

***

3) in section (6), add the following subsections after "the Minister responsible for Climate Change must consult with iwi";

***

> 5) the Minister responsible for Climate Change should act on the advice given by the Independent Climate Commission during the planning process for the Carbon Budget and Policy.
> 
> 6) if the Minister responsible for Climate Change does not act on this advice, they should provide a statement outlining why to the House of Representatives at the next possible opportunity.

***

**7. Section 7 amended (Independent Climate Commission)**

1) in Section (7) replace “ten experts appointed by Parliament” with “eleven experts appointed by Parliament”

2) in Subsection (3) replace “Te Tiriti o Waitangi, tikanga Māori, and Māori interests” with “Te Tiriti o Waitangi, tikanga Māori, and Māori interests; and”

3) in Subsection (3) add the following paragraph after “Te Tiriti o Waitangi, tikanga Māori, and Māori interests; and”:

***

> i) employment, workforce, and labour rights.

***

4) in subsection (6) paragraph (a) replace “2050” with “2035”

5) in Section (7), replace "the Minister for Climate Change should respond to both reports" with 

***

> "the Minister responsible for Climate Change should respond to and enact the advice of both reports. If they do not wish to follow this advice, they should follow the same process outlined in Section 6, subsection 6."

***

6) in Section (7), add the following subsection after "If they do not wish to follow this advice, they should follow the same process outlined in Section 6, subsection 6.";

***
                
> 9) the Independent Climate Commission will set the figures on net carbon and gross carbon reduction targets over the 5 year period to be outlined in the termly government Carbon Budget and Policy Statement.

***
