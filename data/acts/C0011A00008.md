---
title: Criminal Age Of Responsibility Act 2020
bill: 273
introducer: Toastinrussian
author:
- LeChevalierMal-Fait (Forwards!)
party: Forwards!
portfolio: Member's Bills
assent: 2020-05-24
commencement: 2020-05-24
first_reading: reddit.com/r/ModelNZParliament/comments/gc1qmh/b273_criminal_age_of_responsibility_bill_first/
committee: reddit.com/r/ModelNZParliament/comments/ghi5w3/b273_criminal_age_of_responsibility_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/glajnq/b273_criminal_age_of_responsibility_bill_final/
amends:
- Oranga Tamariki Act 1989
---

# Criminal Age Of Responsibility Act 2020

**1. Title**

This Act may be cited as the Criminal Age Of Responsibility Act 2020.

**2. Commencement**

This Act comes into force after receiving Royal Assent

**3. Purpose**

The purpose of this Act is to raise the age of criminal responsibility to 12 and to give police powers to while ensuring the wellbeing of children who pose a threat to other persons..

**4. Interpretation**

In this Act—

**constable** means a Police employee who— 

> a) holds the office of constable appointed under the Police Act 1958 or the Policing Act 2006; and 
> 
> b) includes a constable who holds any level of position within the New Zealand Police.

**child** means an individual under the age of 12.

**parent** with respect to a child means a person who has a day-to-day duty of care towards that child.

**supporter** means a person over the age of 18 whom the child wishes to accompany them in the interview. 

**advocacy worker** means a legal services provider who assists children in the course of interviews granted under section 11. 

**unaccompanied** means a child being interviewed with neither a supporter nor an advocacy worker is present. 

**investigative interview** is a police interview questioning a suspect in a criminal case.

**5. Act to bind the Crown**

This Act binds the Crown.

## Part 1 - Criminal Responsibility

**6. Criminal Responsibility**

1) A child under the age of 12 years cannot commit an offence. 

2) Any previous age of responsibility continues to have effect in relation to any offence committed before subsection (1) comes into force.

3) Nothing in this section invalidates any enactment which applies an age of greater than 12 to an offence.

## Part 2 - Police Powers

**7. Power to take a child under 12 to a place of safety**

1) This section applies where a Constable finds in any place a child under 12 years of age who the constable has reasonable grounds to believe is behaving in such a way or is likely to behave in such a way that is causing or risks causing harm to another person. 

2) The Constable may take the child to a place of safety and keep the child there if the constable is satisfied that is necessary to protect any other person from harm. 

3) As soon as is practicable, once the child is in a place of safety the constable must inform the parent of the child. 

4) A child may be kept in a place of safety under this section— 

> a) only as long as is necessary— 
> 
> > i) to put in place arrangements for the care or protection of the child, or 
> > 
> > ii) for an order under section 3, or 
> > 
> > iii) for an order under section 7. 
> 
> b) for no longer than 24 hours. 

**8. Power to search children under 12**

1) If a constable has a reasonable belief that a child under 12 has committed an act that would be an offence if they were over 12 then the constable may carry out a search without warrant. 

2) This section applies to any enactment which a constable may under the terms of the enactment stop and search a person. 

3) Subsection (1) does not apply to any part of the enactment to the extent that it— 

> a) would allow the constable to arrest the child, or 
> 
> b) provides that the child commits an offence if the child— 
> 
> > i) fails to comply with a requirement of the enactment, or 
> > 
> > ii) obstructs the constable. or 
> 
> c) provides that the constable may apply for a warrant. 

4) The Governor General may by orders in council made on the advice of Ministers, may specify enactments which this section does not apply. In which case the age qualification of the offence is that specified in the enactment.

**9. Order authorising a search in relation to child under 12**

1) A constable may apply to a judge for an order authorising a search in relation to a child under 12 years. 

2) The judge may determine the application after such inquiry or hearing as the judge considers appropriate. 

3) Before determining the application, the judge must consider whether any of the following persons should have an opportunity to make a representation. 

> a) the applicant, 
> 
> b) the child in respect of whom the application was made, 
> 
> c) a parent of the child, 
> 
> d) any other person the judge considers to have an interest in the application. 

4) The judge may determine to grant an order authorising— 

> a) the search of the child, 
> 
> b) the entry to and search of any premises where there cause to suspect that evidence of child neglect or abuse may be uncovered there, 
> 
> c) the entry to and search of any vehicles where there cause to suspect that evidence of child neglect or abuse may be uncovered there, 
> 
> d) the search of anything the constable may find in the course of (a) to (c). 

To occur in a period of seven days beginning on the day the order is made. 

5) The judge may make an order authorising an action in (4) if satisfied there are reasonable grounds to suspect that— 

> a) the child— 
> 
> > i) by behaving in a violent or dangerous way has caused physical harm or risked physical harm to another person, or 
> > 
> > ii) by behaving in a sexually violent or sexually coercive way, has caused or risked causing harm to another person. 
> 
> b) evidence relevant to the investigation of that behaviour may be found in a place. 

6) Once an order has been made— 

> a) as soon as is practicable the parents of the child must be contacted informing them of the order. 
> 
> b) the child must be informed in language appropriate for them, of the order and it’s meaning.

**10. Limitation on police questioning of children under 16**

It is unlawful for a constable to question a child under 16 or for a child to be subject to an investigative interview in relation to an incident which— 

> a) occurred when the child was under 12, and 
> 
> b) was an incident where the child— 
> 
> > i) by behaving in a violent or dangerous way has caused physical harm or risked physical harm to another person, or 
> > 
> > ii) by behaving in a sexually violent or sexually coercive way, has caused or risked causing harm to another person. Unless— Under an order granted under section 6

**11. Child interview orders**

1) A constable may apply to a judge for an order authorising an interview or a child under 16.

2) The judge may determine the application after such inquiry or hearing as the judge considers appropriate. 

3) Before determining the application, the judge must consider whether any of the following persons should have an opportunity to make a representation. 

> a) the applicant, 
> 
> b) the child in respect of whom the application was made, 
> 
> c) a parent of the child, 
> 
> d) any other person the judge considers to have an interest in the application. 

4) The judge may make an order if satisfied there are reasonable grounds to suspect that— 

> a) the child— 
> 
> > i) by behaving in a violent or dangerous way has caused physical harm or risked physical harm to another person, or 
> > 
> > ii) by behaving in a sexually violent or sexually coercive way, has caused or risked causing harm to another person. 
> 
> b) an interview is necessary for a proper investigation of the incident including whether a person other than the child committed an offence. 

**12. Conduct of interview**

1) The child has the following rights during an interview ordered under section 11 of this act— 

> a) The right to not answer questions, 
> 
> b) the right to have a supporter present, 
> 
> c) the right to have an advocacy worker present, and 
> 
> d) the right to not be questioned unaccompanied. 

2) An interview granted under section 6 may only be conducted if these rights are respected. 

3) Subsection (1) (b) does not apply to a supporter whom the interviewer believes to be a threat to the wellbeing of the child. 

**13. Requirements in regard exercise of powers by constables**

1) A constable who enters any unoccupied premises by virtue of an order under this act must leave the premises as effectively secured against unauthorised entry as the constable found them.

2) A constable who is authorised to take any action under this act is also authorised to use reasonable force.

3) A constable before deciding to use reasonable force must take all reasonable steps to obtain the cooperation of the child.

4) A constable may use reasonable force in relation to a child under 12 only as a last resort.

5) The force used by the constable must be no more than necessary to achieve a lawful purpose, and after such a use the constable must attempt to explain to the child why force was used.
