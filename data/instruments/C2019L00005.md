---
title: Customs Legislation Amendment (Kingdom of Morocco) Regulations 2019
maker: ElectrumNS
made: 2019-03-06
urls:
- https://docs.google.com/document/d/1kMOR2Kg1ZtJINm2geO0rEpdcktv2pGwR98jOH24R5yY/edit
- https://www.reddit.com/r/AustraliaSimLower/comments/axxwb0/customs_legislation_amendment_democratic/
made_under: Customs Act 1901
amends:
- Customs (Prohibited Exports) Regulations 1958
---
